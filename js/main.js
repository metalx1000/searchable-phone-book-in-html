$("document").ready(function(){
  createList();
});

function myFunction() {
  var input, filter, ul, li, a, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  ul = document.getElementById("myUL");
  li = ul.getElementsByTagName("li");
  for (i = 0; i < li.length; i++) {
    a = li[i].getElementsByTagName("a")[0];
    txtValue = a.textContent || a.innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      li[i].style.display = "";
    } else {
      li[i].style.display = "none";
    }
  }
}

function createList(){
  $.get("names.php",function(names){
    var names = names.split("\n");
    for(var i in names){
      var name = names[i];
      console.log(name);
      $("#myUL").append("<li><a href=\"#"+name+"\">"+name+"</a></li>");
    }
  });
}
